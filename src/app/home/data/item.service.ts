import { Injectable } from '@angular/core';
import { IItem } from './models/IItem';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ItemService {
  private url: string;
  constructor(private http: HttpClient) {
    this.url = '/api';
  }
  fetchItems(): Observable<IItem[]> {
    return this.http.get<IItem[]>(this.url + '/items');
  }
  createItem(item: IItem): Observable<IItem> {
    return this.http.post<IItem>(this.url + '/items', item);
  }
  updateItem(item: IItem): Observable<IItem> {
    return this.http.put<IItem>(this.url + '/items/' + item._id, item);
  }
  deleteItem(item: IItem): Observable<IItem> {
    return this.http.delete<IItem>(this.url + '/items/' + item._id);
  }
}

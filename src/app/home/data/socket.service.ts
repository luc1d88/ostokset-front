import { Socket } from 'ng-socket-io';
import { Injectable } from '@angular/core';
import { IList } from './models/IList';
import { IListItem } from './models/IListItem';

@Injectable()
export class SocketService {
  private url: string;

  constructor(private socket: Socket) {
    this.url = '/api';
  }

  joinRoom(list: IList) {
    this.socket.emit('join', { 'room': list._id });
  }

  leaveRoom(list: IList) {
    this.socket.emit('leave', { 'room': list._id });
  }

  removeListeners() {
    this.socket.removeAllListeners('new-listItem');
    this.socket.removeAllListeners('update-listItem');
    this.socket.removeAllListeners('remove-listItem');
  }

  getNewItems() {
    return this.socket
    .fromEvent('new-listItem');
  }

  getUpdatedItems() {
    return this.socket
    .fromEvent('update-listItem');
  }

  getRemovedItems() {
    return this.socket
    .fromEvent('remove-listItems');
  }
}

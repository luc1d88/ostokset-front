import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICategory } from './models/ICategory';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CategoryService {
  private url: string;
  private httpOptions: any;

  constructor(
    private http: HttpClient) {
    this.url = '/api';
  }

  fetchCategories(): Observable<ICategory[]> {
    return this.http.get<ICategory[]>(this.url + '/categories');
  }
  createCategory(category: ICategory): Observable<ICategory> {
    return this.http.post<ICategory>(this.url + '/categories', category);
  }
  updateCategory() {

  }
  deleteCategory() {

  }

}

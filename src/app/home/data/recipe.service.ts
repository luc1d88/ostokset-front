import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { IRecipe } from './models/IRecipe';
import { IRecipeItem } from './models/IRecipeItem';

@Injectable()
export class RecipeService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = '/api';
  }

  fetchRecipes() {
    return this.http.get<IRecipe[]>(this.url + '/recipes');
  }
  fetchRecipeById(recipeId: string) {
    return this.http.get<IRecipe>(this.url + '/recipes/' + recipeId);
  }
  createRecipe(recipe: IRecipe) {
    return this.http.post<IRecipe>(this.url + '/recipes', recipe);
  }
  updateRecipe(recipe: IRecipe) {
    return this.http.put<IRecipe>(this.url + '/recipes/' + recipe._id, recipe);
  }
  deleteRecipe(recipe: IRecipe) {
    return this.http.delete<IRecipe>(this.url + '/recipes/' + recipe._id);
  }

  createRecipeItem(recipeItem: IRecipeItem) {
    return this.http.post<IRecipe>(this.url + '/recipes/' + recipeItem.recipe + '/items', recipeItem);
  }
  deleteRecipeItem(recipeItem: IRecipeItem) {
    return this.http.delete<IRecipe>(this.url + '/recipes/' + recipeItem.recipe + '/items/' + recipeItem._id);
  }
  updateRecipeItem(recipeItem: IRecipeItem) {
    return this.http.put<IRecipe>(this.url + '/recipes/' + recipeItem.recipe + '/items/' + recipeItem._id , recipeItem);
  }
}

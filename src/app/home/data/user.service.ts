import { Injectable } from '@angular/core';
import { IUser } from './models/IUser';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = '/api';
  }
  deleteUser(user: IUser) {
    return this.http.delete(this.url + '/users/' + user._id);
  }
  getTermsOfUse(user: IUser) {
    return this.http.get(this.url + '/users/' + user._id + '/terms');
  }
  updateTermsOfUse(user: IUser, approved: boolean) {
    return this.http.put(this.url + '/users/' + user._id + '/terms', { approvedTermsOfUse: approved });
  }
}

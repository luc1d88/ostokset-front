import { IList } from './IList';

export interface IRemoveList {
  list: IList;
  ownList: boolean;
}

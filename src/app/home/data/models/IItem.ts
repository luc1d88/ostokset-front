export interface IItem {
  _id?: string;
  name: string;
  category: any;
  owner?: string;
  restricted?: boolean;
}

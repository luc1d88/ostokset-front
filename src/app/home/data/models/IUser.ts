import { IList } from './IList';

export interface IUser {
  _id?: string;
  displayName: string;
  email: string;
  profilePicture: string;
}

export interface IListShare {
  _id?: string;
  list: IList;
  user: IUser;
}

export interface IListItem {
  _id?: string;
  quantity: number;
  priority: string;
  checked: boolean;
  item?: any;
  list?: any;
}

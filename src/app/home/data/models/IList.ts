export interface IList {
  _id?: string;
  name: string;
  description?: string;
  owner?: any;
}

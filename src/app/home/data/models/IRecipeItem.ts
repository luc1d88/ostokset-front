
export interface IRecipeItem {
  _id?: string;
  quantity?: string;
  item?: any;
  recipe?: any;
  group?: string;
}

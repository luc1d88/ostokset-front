export interface IRecipe {
    _id?: string;
    name: string;
    category: string;
    items: any[];
    instructions: string;
    owner?: any;
  }

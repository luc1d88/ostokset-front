export interface ICategory {
    _id?: string;
    name: string;
    owner?: string;
    restricted?: boolean;
}

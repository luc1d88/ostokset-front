import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { IList } from './models/IList';
import { IUser, IListShare } from './models/IUser';
import { IListItem } from './models/IListItem';

import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/map';


@Injectable()
export class ListService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = '/api';
  }

  fetchListById(list: string) {
    return this.http.get<IList>(this.url + '/lists/' + list);
  }
  fetchLists() {
    return Observable.forkJoin(
      this.http.get<IList[]>(this.url + '/lists'),
      this.http.get<IList[]>(this.url + '/lists/shared')
    );
  }
  fetchListItems(list: IList) {
    return this.http.get<IListItem>(this.url + '/lists/' + list._id + '/items');
  }
  createListItem(listItem: IListItem) {
    return this.http.post<IListItem>(this.url + '/lists/' + listItem.list + '/items', listItem);
  }
  addSharedList(sharedList: any) {
    return this.http.post(this.url + '/lists/' + sharedList.friendCode, null);
  }
  updateListItem(listItem: IListItem) {
    return this.http.put<IListItem>(this.url + '/lists/' + listItem.list + '/items/' + listItem._id, listItem);
  }
  fetchUserLists(): Observable<IList[]> {
    return this.http.get<IList[]>(this.url + '/lists');
  }
  fetchSharedLists(): Observable<IList[]> {
    return this.http.get<IList[]>(this.url + '/lists/shared');
  }
  createList(list: IList): Observable<IList> {
    return this.http.post<IList>(this.url + '/lists', list);
  }
  updateList(list: IList) {
    return this.http.put<IList>(this.url + '/lists/' + list._id, list);
  }
  deleteList(list: IList) {
    return this.http.delete<IList>(this.url + '/lists/' + list._id);
  }
  deleteSharedList(list: IList) {
    return this.http.delete<IList>(this.url + '/lists/shared/' + list._id);
  }
  removeCheckedListItems(list: IList) {
    return this.http.delete<IListItem[]>(this.url + '/lists/' + list._id + '/items');
  }
  fetchUsersInTheList(list: IList) {
    return this.http.get<IListShare[]>(this.url + '/lists/' + list._id + '/users');
  }
}

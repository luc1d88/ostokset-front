import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { AuthService } from '../../auth/auth.service';
import { MatDialogRef, MatDialog } from '@angular/material';
import { RemoveUserDialogComponent } from './remove-user-dialog/remove-user-dialog.component';
import { TermsDialogComponent } from '../../common/terms-dialog/terms-dialog.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public user: any;
  private removeListDialogRef: MatDialogRef<RemoveUserDialogComponent>;
  private termsOfUseDialogRef: MatDialogRef<TermsDialogComponent>;

  constructor(private storage: LocalStorageService,
    private dialog: MatDialog,
    private authService: AuthService) { }

  public logout() {
    this.authService.logout();
  }
  ngOnInit() {
    this.user = this.storage.retrieve('profile');
  }
  public openRemoveUser(user: any): void {
    this.removeListDialogRef = this.dialog.open(RemoveUserDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: false
    });
    this.removeListDialogRef.afterClosed().subscribe((result) => {
      if (result) { this.authService.logout(); }
    });
  }

  public openTermsOfUse(user: any): void {
    this.termsOfUseDialogRef = this.dialog.open(TermsDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: true,
      data: user
    });
    this.termsOfUseDialogRef.afterClosed().subscribe((result) => {
      if (!result) {
        this.openRemoveUser(this.user);
      }
    });
  }

}

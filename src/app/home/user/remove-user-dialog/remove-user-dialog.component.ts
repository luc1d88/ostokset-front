import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IUser } from '../../data/models/IUser';
import { UserService } from '../../data/user.service';

@Component({
  selector: 'app-remove-user-dialog',
  templateUrl: './remove-user-dialog.component.html',
  styleUrls: ['./remove-user-dialog.component.scss']
})
export class RemoveUserDialogComponent implements OnInit {
  private user: IUser;
  public deleteUser: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IUser,
    private dialogRef: MatDialogRef<RemoveUserDialogComponent>,
    private userService: UserService) {
      this.deleteUser = { delete: false };
      this.user = Object.assign({}, data);
    }

  ngOnInit() { }

  close(): void {
    this.dialogRef.close();
  }

  onDelete(): void {
    this.userService.deleteUser(this.user).subscribe((user: IUser) => {
      this.dialogRef.close(user);
    });
  }

}

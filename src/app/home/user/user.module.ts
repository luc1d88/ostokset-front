import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { CommonCustomModule } from '../../common/common-custom.module';
import { MaterialModule } from '../../material.module';
import { RemoveUserDialogComponent } from './remove-user-dialog/remove-user-dialog.component';
import { FormsModule } from '@angular/forms';
import { UserService } from '../data/user.service';

@NgModule({
  imports: [
    CommonModule,
    CommonCustomModule,
    FormsModule,
    MaterialModule
  ],
  declarations: [
    UserComponent,
    RemoveUserDialogComponent
  ],
  entryComponents: [
    RemoveUserDialogComponent
  ]
})
export class UserModule {
  static forRoot() {
    return {
      ngModule: UserModule,
      providers: [UserService]
    };
  }
}

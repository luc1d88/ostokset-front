import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../data/recipe.service';
import { IRecipe } from '../data/models/IRecipe';
import * as _ from 'lodash';
import { MatDialogRef, MatDialog } from '@angular/material';
import { AddRecipeDialogComponent } from './add-recipe-dialog/add-recipe-dialog.component';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { RemoveRecipeDialogComponent } from './remove-recipe-dialog/remove-recipe-dialog.component';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent implements OnInit {

  private recipes: IRecipe[];
  private addRecipeDialogRef: MatDialogRef<AddRecipeDialogComponent>;
  private removeRecipeDialogRef: MatDialogRef<RemoveRecipeDialogComponent>;

  public viewRecipes: any[];

  constructor(private recipeService: RecipeService,
    private authService: AuthService,
    private dialog: MatDialog,
    private router: Router) {
    this.viewRecipes = [];
  }

  ngOnInit() {
    this.getRecipes();
  }

  public logout() {
    this.authService.logout();
  }

  private getRecipes(): void {
    this.recipeService.fetchRecipes()
    .subscribe((recipes) => {
      this.recipes = recipes;
      this.viewRecipes = this.map(this.recipes);
    });
  }

  private map(recipes: IRecipe[]): any[] {
    const categories = _.map(_.uniqBy(recipes, 'category'), item => {
      return {'name': item.category };
    });
    return _.forEach(categories, (category: any) => {
      category.recipes =  _.filter(recipes, { 'category' : category.name });
    });
  }

  openAddRecipe(): void {
    this.addRecipeDialogRef = this.dialog.open(AddRecipeDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: false
    });
    this.addRecipeDialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.recipes.push(result);
        this.viewRecipes = this.map(this.recipes);
      }
    });
  }

  openRemoveRecipe(recipe: IRecipe): void {
    this.removeRecipeDialogRef = this.dialog.open(RemoveRecipeDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: false,
      data: recipe
    });
    this.removeRecipeDialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.recipes = _.filter(this.recipes, (_recipe: IRecipe) =>  _recipe._id !== result._id );
        this.viewRecipes = this.map(this.recipes);
      }
    });
  }

  navigateToRecipe(recipe: IRecipe) {
    this.router.navigate(['home/recipes/' + recipe._id]);
  }

}

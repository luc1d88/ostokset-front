import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IRecipe } from '../../data/models/IRecipe';
import { RecipeService } from '../../data/recipe.service';

@Component({
  selector: 'app-remove-recipe-dialog',
  templateUrl: './remove-recipe-dialog.component.html',
  styleUrls: ['./remove-recipe-dialog.component.scss']
})
export class RemoveRecipeDialogComponent implements OnInit {
  public recipe: IRecipe;
  public deleteRecipe: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: IRecipe,
    private dialogRef: MatDialogRef<RemoveRecipeDialogComponent>,
    private recipeService: RecipeService) {
      this.deleteRecipe = { delete: false };
      this.recipe = Object.assign({}, data);
   }

  ngOnInit() {}

  close(): void {
    this.dialogRef.close();
  }

  onDelete(): void {
    this.recipeService.deleteRecipe(this.recipe).subscribe((recipe) => {
      this.dialogRef.close(this.recipe);
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { IRecipe } from '../../data/models/IRecipe';
import { MatDialogRef } from '@angular/material';
import { RecipeService } from '../../data/recipe.service';
import { ICategory } from '../../data/models/ICategory';

@Component({
  selector: 'app-add-recipe-dialog',
  templateUrl: './add-recipe-dialog.component.html',
  styleUrls: ['./add-recipe-dialog.component.scss']
})
export class AddRecipeDialogComponent implements OnInit {
  public recipe: IRecipe;

  constructor(
    private dialogRef: MatDialogRef<AddRecipeDialogComponent>,
    private recipeService: RecipeService) {
    this.recipe = { name: '', category: '', items: [], instructions: '' };
  }
  add(recipe: IRecipe) {
    this.recipeService.createRecipe(recipe).subscribe((_recipe) => {
      this.recipe = _recipe;
      this.dialogRef.close(this.recipe);
    });
  }
  close(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}

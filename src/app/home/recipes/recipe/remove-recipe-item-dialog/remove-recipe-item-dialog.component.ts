import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IRecipeItem } from '../../../data/models/IRecipeItem';
import { RecipeService } from '../../../data/recipe.service';

@Component({
  selector: 'app-remove-recipe-item-dialog',
  templateUrl: './remove-recipe-item-dialog.component.html',
  styleUrls: ['./remove-recipe-item-dialog.component.scss']
})
export class RemoveRecipeItemDialogComponent implements OnInit {
  public recipeItem;
  public deleteList: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IRecipeItem,
    private dialogRef: MatDialogRef<RemoveRecipeItemDialogComponent>,
    private recipeService: RecipeService) {
      this.deleteList = { delete: false };
      this.recipeItem = Object.assign({}, data);
  }

  ngOnInit() {}

  close(): void {
    this.dialogRef.close();
  }

  onDelete(): void {
    this.recipeService.deleteRecipeItem(this.recipeItem).subscribe((recipeItem: IRecipeItem) => {
      this.dialogRef.close(recipeItem);
    });
  }

}

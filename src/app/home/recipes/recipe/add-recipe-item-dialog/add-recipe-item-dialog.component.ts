import { Component, OnInit, Inject } from '@angular/core';
import { IRecipeItem } from '../../../data/models/IRecipeItem';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RecipeService } from '../../../data/recipe.service';
import { IItem } from '../../../data/models/IItem';

@Component({
  selector: 'app-add-recipe-item-dialog',
  templateUrl: './add-recipe-item-dialog.component.html',
  styleUrls: ['./add-recipe-item-dialog.component.scss']
})
export class AddRecipeItemDialogComponent implements OnInit {
  public recipeItem: IRecipeItem;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IRecipeItem,
    private dialogRef: MatDialogRef<AddRecipeItemDialogComponent>,
    private recipeService: RecipeService) {
      this.recipeItem = data;
  }
  close(): void {
    this.dialogRef.close();
  }
  add(recipeItem: IRecipeItem): void {
    this.recipeService.createRecipeItem(this.recipeItem).subscribe((_recipeItem: IRecipeItem) => {
      this.dialogRef.close(_recipeItem);
    });
  }
  ngOnInit() { }

}

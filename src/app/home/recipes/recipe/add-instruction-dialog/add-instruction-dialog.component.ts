import { Component, OnInit, Inject } from '@angular/core';
import { IRecipe } from '../../../data/models/IRecipe';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RecipeService } from '../../../data/recipe.service';

@Component({
  selector: 'app-add-instruction-dialog',
  templateUrl: './add-instruction-dialog.component.html',
  styleUrls: ['./add-instruction-dialog.component.scss']
})
export class AddInstructionDialogComponent implements OnInit {
  public recipe: IRecipe;
  public instructions: string;
  public max: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IRecipe,
    private dialogRef: MatDialogRef<AddInstructionDialogComponent>,
    private recipeService: RecipeService) {
      this.recipe = Object.assign({}, data);
      this.instructions = this.recipe.instructions;
    }

  ngOnInit() {}

  public close() {
    this.dialogRef.close();
  }

  public add(instructions) {
    this.recipe.instructions = instructions;
    this.recipeService.updateRecipe(this.recipe).subscribe((recipe: IRecipe) => {
      this.dialogRef.close(recipe);
    });
  }

}

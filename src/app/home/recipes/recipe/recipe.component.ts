import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { RecipeService } from '../../data/recipe.service';
import 'rxjs/add/operator/switchMap';
import { IRecipe } from '../../data/models/IRecipe';
import * as _ from 'lodash';
import { IItem } from '../../data/models/IItem';
import { ItemService } from '../../data/item.service';
import { MatDialogRef, MatDialog } from '@angular/material';
import { AddRecipeItemDialogComponent } from './add-recipe-item-dialog/add-recipe-item-dialog.component';
import { IRecipeItem } from '../../data/models/IRecipeItem';
import { EditRecipeItemDialogComponent } from './edit-recipe-item-dialog/edit-recipe-item-dialog.component';
import { RemoveRecipeItemDialogComponent } from './remove-recipe-item-dialog/remove-recipe-item-dialog.component';
import { AddInstructionDialogComponent } from './add-instruction-dialog/add-instruction-dialog.component';
import { AddItemDialogComponent } from '../../items/add-item-dialog/add-item-dialog.component';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {
  public recipe: IRecipe;
  public items: IItem[];
  public viewRecipeItems: any[];
  public filteredItems: IItem[];
  public filterString: string;
  public secondTitle: string;
  public secondIcon: string;
  private addRecipeItemDialogRef: MatDialogRef<AddRecipeItemDialogComponent>;
  private editRecipeItemDialogRef: MatDialogRef<EditRecipeItemDialogComponent>;
  private removeRecipeItemDialogRef: MatDialogRef<RemoveRecipeItemDialogComponent>;
  private addInstructionDialogRef: MatDialogRef<AddInstructionDialogComponent>;
  private addItemDialogRef: MatDialogRef<AddItemDialogComponent>;

  @ViewChild('input') inputElementRef: ElementRef;

  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private itemService: ItemService,
    private recipeService: RecipeService) {
    this.recipe = { name: '', items: [], category: '', instructions: '' };
    this.viewRecipeItems = [];
    this.items = [];
    this.secondTitle = '';
    this.secondIcon = '';
  }

  ngOnInit() {
    this.route.paramMap
    .switchMap((params: ParamMap) => {
      return this.recipeService.fetchRecipeById(params.get('id'));
    }).subscribe((recipe) => {
      this.recipe = recipe;
      this.viewRecipeItems = this.map(this.recipe);
    });
    this.getItems();
  }

  private getItems(): void {
    this.itemService.fetchItems().subscribe((items) => {
      this.items = items;
    });
  }

  private map(recipe: IRecipe): any[] {
    const groups = _.map(_.uniqBy(recipe.items, 'group'), item => {
      return { 'group': item.group };
    });
    return _.forEach(groups, (group: any) => {
      group.items =  _.filter(recipe.items, { 'group' : group.group });
    });
  }

  filter(filter): void {
    this.filteredItems = this.items;
    if (filter.length < 1) {
      this.filteredItems = this.items;
    } else {
      this.filteredItems = _.filter(this.filteredItems, item => item.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
    }
  }

  public openAddRecipeItem(item: IItem): void {
    const recipeItem: IRecipeItem = { item: item, recipe: this.recipe._id, quantity: '', group: '' };

    this.addRecipeItemDialogRef = this.dialog.open(AddRecipeItemDialogComponent, {
      width: '400px',
      closeOnNavigation: true,
      disableClose: true,
      data: recipeItem,
      autoFocus: false
    });
    this.addRecipeItemDialogRef.afterClosed().subscribe((result: IItem) => {
      if (result) {
        this.inputElementRef.nativeElement.blur();
        this.recipe.items.push(result);
        this.viewRecipeItems = this.map(this.recipe);
      }
    });
  }

  public openEditRecipeItem(recipeItem: IRecipeItem): void {
    this.editRecipeItemDialogRef = this.dialog.open(EditRecipeItemDialogComponent, {
      width: '400px',
      closeOnNavigation: true,
      disableClose: true,
      data: recipeItem,
      autoFocus: false
    });
    this.editRecipeItemDialogRef.afterClosed().subscribe((result: IRecipeItem) => {
      if (result) { this.viewRecipeItems = this.map(this.recipe); }
    });
  }

  public openRemoveListItem(recipeItem: IRecipeItem): void {
    this.removeRecipeItemDialogRef = this.dialog.open(RemoveRecipeItemDialogComponent, {
      width: '400px',
      closeOnNavigation: true,
      disableClose: true,
      data: recipeItem,
      autoFocus: false
    });
    this.removeRecipeItemDialogRef.afterClosed().subscribe((result: IRecipeItem) => {
      if (result) {
        this.recipeService.fetchRecipeById(this.recipe._id).subscribe((recipe) => {
          this.recipe = recipe;
          this.viewRecipeItems = this.map(this.recipe);
        });
      }
    });
  }

  public openAddInstruction(recipe: IRecipe): void {
    this.addInstructionDialogRef = this.dialog.open(AddInstructionDialogComponent, {
      width: '400px',
      closeOnNavigation: true,
      disableClose: true,
      data: recipe,
      autoFocus: false
    });
    this.addInstructionDialogRef.afterClosed().subscribe((result: IRecipe) => {
      if (result) {
        this.recipe = result;
        this.viewRecipeItems = this.map(this.recipe);
      }
    });
  }

  openAddItem(): void {
    this.addItemDialogRef = this.dialog.open(AddItemDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: false
    });
    this.addItemDialogRef.afterClosed().subscribe((result: IItem) => {
      if (result) {
        this.openAddRecipeItem(result);
      }
    });
  }
}

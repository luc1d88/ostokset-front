import { Component, OnInit, Inject } from '@angular/core';
import { IRecipeItem } from '../../../data/models/IRecipeItem';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RecipeService } from '../../../data/recipe.service';
import { IItem } from '../../../data/models/IItem';

@Component({
  selector: 'app-edit-recipe-item-dialog',
  templateUrl: './edit-recipe-item-dialog.component.html',
  styleUrls: ['./edit-recipe-item-dialog.component.scss']
})
export class EditRecipeItemDialogComponent implements OnInit {
  public recipeItem: IRecipeItem;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IRecipeItem,
    private dialogRef: MatDialogRef<EditRecipeItemDialogComponent>,
    private recipeService: RecipeService) {
      this.recipeItem = Object.assign({}, data);
  }
  close(): void {
    this.dialogRef.close();
  }
  edit(recipeItem: IRecipeItem): void {
    this.recipeService.updateRecipeItem(this.recipeItem).subscribe((_recipeItem: IRecipeItem) => {
      this.dialogRef.close(_recipeItem);
    });
  }
  ngOnInit() { }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { RecipeService } from '../data/recipe.service';
import { RecipeComponent } from './recipe/recipe.component';
import { RecipesComponent } from './recipes.component';
import { CommonCustomModule } from '../../common/common-custom.module';
import { AddRecipeDialogComponent } from './add-recipe-dialog/add-recipe-dialog.component';
import { AddRecipeItemDialogComponent } from './recipe/add-recipe-item-dialog/add-recipe-item-dialog.component';
import { AddInstructionDialogComponent } from './recipe/add-instruction-dialog/add-instruction-dialog.component';
import { EditRecipeItemDialogComponent } from './recipe/edit-recipe-item-dialog/edit-recipe-item-dialog.component';
import { RemoveRecipeItemDialogComponent } from './recipe/remove-recipe-item-dialog/remove-recipe-item-dialog.component';
import { RemoveRecipeDialogComponent } from './remove-recipe-dialog/remove-recipe-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CommonCustomModule,
    MaterialModule
  ],
  entryComponents: [
    AddRecipeDialogComponent,
    AddRecipeItemDialogComponent,
    AddInstructionDialogComponent,
    EditRecipeItemDialogComponent,
    RemoveRecipeItemDialogComponent,
    RemoveRecipeDialogComponent
  ],
  declarations: [
    RecipesComponent,
    RecipeComponent,
    AddRecipeDialogComponent,
    AddRecipeItemDialogComponent,
    AddInstructionDialogComponent,
    EditRecipeItemDialogComponent,
    RemoveRecipeItemDialogComponent,
    RemoveRecipeDialogComponent
  ]
})
export class RecipesModule {
  static forRoot() {
    return {
      ngModule: RecipesModule,
      providers: [
        RecipeService,
      ]
    };
  }
}

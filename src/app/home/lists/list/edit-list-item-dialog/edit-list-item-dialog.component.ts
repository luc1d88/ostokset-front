import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ListService } from '../../../data/list.service';
import { IListItem } from '../../../data/models/IListItem';

@Component({
  selector: 'app-edit-list-item-dialog',
  templateUrl: './edit-list-item-dialog.component.html',
  styleUrls: ['./edit-list-item-dialog.component.scss']
})
export class EditListItemDialogComponent implements OnInit {
  public listItem: IListItem;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private listService: ListService,
    private dialogRef: MatDialogRef<EditListItemDialogComponent>) {
    this.listItem = { quantity: 1, priority: 'medium', checked: false };
    this.listItem = Object.assign({}, data);
  }
  ngOnInit() {}
  edit(listItem) {
    this.listService.updateListItem(listItem).subscribe((_listItem: IListItem) => {
      this.listItem = _listItem;
      this.dialogRef.close(this.listItem);
    });
  }
  close(): void {
    this.dialogRef.close();
  }
}

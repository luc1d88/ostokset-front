import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IUser } from '../../../data/models/IUser';
import { UserService } from '../../../data/user.service';

@Component({
  selector: 'app-user-details-dialog',
  templateUrl: './user-details-dialog.component.html',
  styleUrls: ['./user-details-dialog.component.scss']
})
export class UserDetailsDialogComponent implements OnInit {
  public showButtons: boolean;
  public userBlocked: boolean;
  public user: IUser;

  constructor(private dialogRef: MatDialogRef<UserDetailsDialogComponent>,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.showButtons = data.showButtons;
    this.userBlocked = data.userBlocked;
    this.user = data.user;
  }
  close(): void {
    this.dialogRef.close();
  }
  block(): void {
    console.log('block user');
  }
  allow(): void {
    console.log('allow user');
  }
  ngOnInit() {}

}

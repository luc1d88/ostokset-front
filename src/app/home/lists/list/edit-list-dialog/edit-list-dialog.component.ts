import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IList } from '../../../data/models/IList';
import { ListService } from '../../../data/list.service';

@Component({
  selector: 'app-edit-list-dialog',
  templateUrl: './edit-list-dialog.component.html',
  styleUrls: ['./edit-list-dialog.component.scss']
})
export class EditListDialogComponent implements OnInit {
  public list: IList;

  constructor(private dialogRef: MatDialogRef<EditListDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IList,
    private listService: ListService) {
      this.list = data;
    }

  ngOnInit() {}
  close(): void {
    this.dialogRef.close();
  }
  edit(list: IList): void {
    this.listService.updateList({ _id: list._id, name: list.name, description: list.description }).subscribe((_list: IList) => {
      this.list = _list;
      this.dialogRef.close(this.list);
    });
  }

}

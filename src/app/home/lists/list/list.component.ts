import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import * as _ from 'lodash';
import { ListService } from '../../data/list.service';
import { IList } from '../../data/models/IList';
import { IItem } from '../../data/models/IItem';
import { ItemService } from '../../data/item.service';
import { IListItem } from '../../data/models/IListItem';
import { MatAutocompleteSelectedEvent, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { ICategory } from '../../data/models/ICategory';
import { CategoryService } from '../../data/category.service';
import { EditListItemDialogComponent } from './edit-list-item-dialog/edit-list-item-dialog.component';
import { SocketService } from '../../data/socket.service';
import { EditListDialogComponent } from './edit-list-dialog/edit-list-dialog.component';
import { LoadingDialogComponent } from '../../../common/loading-dialog/loading-dialog.component';
import { IUser, IListShare } from '../../data/models/IUser';
import { AddItemDialogComponent } from '../../items/add-item-dialog/add-item-dialog.component';
import { LocalStorageService } from 'ngx-webstorage';
import { SocketNotificationComponent } from '../../../common/socket-notification/socket-notification.component';
import { UserDetailsDialogComponent } from './user-details-dialog/user-details-dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  public list: IList;
  public items: IItem[];
  public listShares: IListShare[];
  public filteredItems: IItem[];
  public filter: string;
  public listItems: any[];
  public viewListItems: any[];
  public sharedUsersClosed = true;
  private user: IUser;
  private editListItemDialogRef: MatDialogRef<EditListItemDialogComponent>;
  private editListDialogRef: MatDialogRef<EditListDialogComponent>;
  private loadingDialogRef: MatDialogRef<LoadingDialogComponent>;
  private addItemDialogRef: MatDialogRef<AddItemDialogComponent>;
  private userDetailsDialogRef: MatDialogRef<UserDetailsDialogComponent>;

  @ViewChild('input') inputElementRef: ElementRef;

  constructor(
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private storage: LocalStorageService,
    private listService: ListService,
    private categoryService: CategoryService,
    private itemService: ItemService,
    private socketService: SocketService,
    private router: Router) {
      this.list = { name: '', description: ''};
      this.filteredItems = [];
      this.listShares = [];
      this.viewListItems = [];
  }
  ngOnInit() {
    this.user = this.storage.retrieve('profile');
    setTimeout(() => {
      this.openLoading();
    }, 0);
    this.itemService.fetchItems().subscribe((items: IItem[]) => {
      this.items = items;
      this.filteredItems = this.items;
    });
    this.route.paramMap
    .switchMap((params: ParamMap) => {
      return this.listService.fetchListById(params.get('id'));
      }).flatMap((list: IList) => {
        this.list = list;
        return this.listService.fetchListItems(list);
      }).subscribe((listItems: any) => {
        this.listItems = listItems;
        this.viewListItems = this.mapItems(this.listItems);
        this.socketService.joinRoom(this.list);
        this.closeLoading();
        this.listService.fetchUsersInTheList(this.list).subscribe((listShares) => {
          this.listShares = listShares;
        });
      });
    this.subscibeNewItems();
    this.subscribeRemovedItems();
    this.subscribeUpdatedItems();
  }
  private subscribeUpdatedItems() {
    this.socketService
    .getUpdatedItems()
    .subscribe((change: any) => {
      if (change.user._id !== this.user._id) {
        this.openNotificationSnackBar({ user: change.user, message: 'On päivittänyt tuottetta'});
        const original = _.find(this.listItems, (__listItem: IListItem) => {
          if (__listItem._id === change.listItem._id) {
            return change.listItem;
          }
        });
        this.listItems[_.indexOf(this.listItems, original)] = change.listItem;
        this.viewListItems = this.mapItems(this.listItems);
      }
    });
  }
  private subscribeRemovedItems() {
    this.socketService
    .getRemovedItems()
    .subscribe((change: any) => {
      if (change.user._id !== this.user._id) {
        this.openNotificationSnackBar({ user: change.user, message: 'On poistanut valmiit tuotteet'});
        this.listItems = change.listItems;
        this.viewListItems = this.mapItems(this.listItems);
      }
    });
  }
  private subscibeNewItems() {
    this.socketService
    .getNewItems()
    .subscribe((change: any) => {
      if (change.user._id !== this.user._id) {
        this.openNotificationSnackBar({ user: change.user, message: 'On lisännyt uuden tuotteen'});
        this.listItems.push(change.listItem);
        this.viewListItems = this.mapItems(this.listItems);
      }
    });
  }
  ngOnDestroy() {
    this.socketService.leaveRoom(this.list);
    this.socketService.removeListeners();
  }
  public addListItem(item: IItem) {
    const listItem: IListItem = {
      item: item._id,
      list: this.list._id,
      quantity: 1,
      priority: 'medium',
      checked: false
    };
    this.listService.createListItem(listItem).subscribe((_listItem: IListItem) => {
      this.filter = '';
      this.inputElementRef.nativeElement.blur();
      this.listItems.push(_listItem);
      this.viewListItems = this.mapItems(this.listItems);
    });
  }
  private mapItems(listItems): any[] {
    const _listItems = _.map(listItems, item => Object.assign({}, item));
    const unique = _.map(_.uniqBy(_listItems, 'item.category._id'), listItem => {
      return { name: listItem.item.category.name, _id: listItem.item.category._id };
    });
    return  _.forEach(unique, (category: any) => {
      category.closed = false;
      category.items = [];
      _.forEach(_listItems, listItem => {
        if (listItem.item.category._id === category._id ) {
          category.items.push(listItem);
        }
      });
    });
  }
  public toggle(listItem: IListItem) {
    listItem.checked = !listItem.checked;
    this.listService.updateListItem(listItem).subscribe((_listItem: IListItem) => {
      const original = _.find(this.listItems, (__listItem: IListItem) => {
        if (__listItem._id === _listItem._id) {
          return _listItem;
        }
      });
      this.listItems[_.indexOf(this.listItems, original)] = _listItem;
      this.viewListItems = this.mapItems(this.listItems);
    });
  }
  public openAddItemDialog(): void {
    this.addItemDialogRef = this.dialog.open(AddItemDialogComponent, {
      width: '400px',
      closeOnNavigation: true,
      disableClose: true,
      data: this.list,
      autoFocus: false
    });
    this.addItemDialogRef.afterClosed().subscribe((result: IItem) => {
      if (result) {
        this.addListItem(result);
      }
    });
  }
  public openEditListDialog(): void {
    this.editListDialogRef = this.dialog.open(EditListDialogComponent, {
      width: '400px',
      closeOnNavigation: true,
      disableClose: true,
      data: Object.assign({}, this.list),
      autoFocus: false
    });
    this.editListDialogRef.afterClosed().subscribe((result: IList) => {
      if (result) { this.list = result; }
    });
  }
  public openEditListItemDialog(listItem): void {
    this.editListItemDialogRef = this.dialog.open(EditListItemDialogComponent, {
      width: '400px',
      closeOnNavigation: true,
      disableClose: true,
      data: Object.assign({}, listItem),
      autoFocus: false
    });
    this.editListItemDialogRef.afterClosed().subscribe((result: IListItem) => {
      const original = _.find(this.listItems, (__listItem: IListItem) => {
        if (__listItem._id === result._id) {
          return result;
        }
      });
      this.listItems[_.indexOf(this.listItems, original)] = result;
      this.viewListItems = this.mapItems(this.listItems);
    });
  }
  public openUserDetailsDialog(user: any): void {
    this.userDetailsDialogRef = this.dialog.open(UserDetailsDialogComponent, {
      width: '400px',
      closeOnNavigation: true,
      disableClose: false,
      data: {
        user: user.user,
        userBlocked: user.blocked,
        showButtons: this.list.owner === this.user._id ? true : false
      },
      autoFocus: false
    });
  }
  public removeChecked(): void {
    this.listService.removeCheckedListItems(this.list).subscribe((listItems: IListItem[]) => {
      this.listItems = listItems;
      this.viewListItems = this.mapItems(this.listItems);
    });
  }
  public filterItems(filter): void {
    this.filteredItems = this.items;
    if (filter.length < 1) {
      this.filteredItems = this.items;
    } else {
      this.filteredItems = _.filter(this.filteredItems, item => item.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
    }
  }
  private openLoading(): void {
    this.loadingDialogRef = this.dialog.open(LoadingDialogComponent, {
      closeOnNavigation: true,
      disableClose: true
    });
  }
  private closeLoading(): void {
    this.loadingDialogRef.close();
  }
  private openNotificationSnackBar(data: any) {
    this.snackBar.openFromComponent(SocketNotificationComponent, {
      data: data,
      duration: 2000
    });
  }
  public onActivationChanged(value: boolean) {
    if (value) {
        this.listService.fetchListItems(this.list)
        .subscribe((listItems: any) => {
          this.listItems = listItems;
          this.viewListItems = this.mapItems(this.listItems);
          this.socketService.joinRoom(this.list);
          this.listService.fetchUsersInTheList(this.list).subscribe((listShares) => {
            this.listShares = listShares;
          });
      });
      this.subscibeNewItems();
      this.subscribeRemovedItems();
      this.subscribeUpdatedItems();
    } else if (!value) {
      this.socketService.leaveRoom(this.list);
      this.socketService.removeListeners();
    }
  }
}

import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ListService } from '../../data/list.service';
import { IList } from '../../data/models/IList';
import { IRemoveList } from '../../data/models/IRemoveList';

@Component({
  selector: 'app-remove-list-dialog',
  templateUrl: './remove-list-dialog.component.html',
  styleUrls: ['./remove-list-dialog.component.scss']
})
export class RemoveListDialogComponent implements OnInit {
  public list: IRemoveList;
  public deleteList: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IRemoveList,
    private dialogRef: MatDialogRef<RemoveListDialogComponent>,
    private listService: ListService) {
      this.deleteList = { delete: false };
      this.list = Object.assign({}, data);
  }

  ngOnInit() {}

  close(): void {
    this.dialogRef.close();
  }

  onDelete(list: IList): void {
    if (this.list.ownList) {
      this.listService.deleteList(list).subscribe((_list: IList) => {
        this.dialogRef.close(list);
      });
    } else {
      this.listService.deleteSharedList(list).subscribe((_list: IList) => {
        this.dialogRef.close(list);
      });
    }
  }

}

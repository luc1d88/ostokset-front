import { Component, OnInit } from '@angular/core';
import { IList } from '../../data/models/IList';
import { MatDialogRef } from '@angular/material';
import { ListService } from '../../data/list.service';

@Component({
  selector: 'app-add-list-dialog',
  templateUrl: './add-list-dialog.component.html',
  styleUrls: ['./add-list-dialog.component.scss']
})
export class AddListDialogComponent implements OnInit {
  public list: IList;

  constructor(private dialogRef: MatDialogRef<AddListDialogComponent>,
    private listService: ListService) {
      this.list = { name: '', description: '' };
    }

  ngOnInit() {
  }
  close(): void {
    this.dialogRef.close();
  }
  add(list: IList): void {
    this.listService.createList({ name: list.name, description: list.description }).subscribe((_list: IList) => {
      this.list = _list;
      this.dialogRef.close(this.list);
    });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CommonCustomModule } from '../../common/common-custom.module';
import { MaterialModule } from '../../material.module';
import { ListsComponent } from './lists.component';
import { ListComponent } from './list/list.component';
import { ListService } from '../data/list.service';
import { AddListDialogComponent } from './add-list-dialog/add-list-dialog.component';
import { AddSharedListDialogComponent } from './add-shared-list-dialog/add-shared-list-dialog.component';
import { EditListItemDialogComponent } from './list/edit-list-item-dialog/edit-list-item-dialog.component';
import { EditListDialogComponent } from './list/edit-list-dialog/edit-list-dialog.component';
import { RemoveListDialogComponent } from './remove-list-dialog/remove-list-dialog.component';
import { SocketService } from '../data/socket.service';
import { UserDetailsDialogComponent } from './list/user-details-dialog/user-details-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CommonCustomModule,
    MaterialModule
  ],
  declarations: [
    ListsComponent,
    ListComponent,
    AddListDialogComponent,
    AddSharedListDialogComponent,
    EditListItemDialogComponent,
    EditListDialogComponent,
    RemoveListDialogComponent,
    UserDetailsDialogComponent,
  ],
  entryComponents: [
    AddListDialogComponent,
    AddSharedListDialogComponent,
    EditListItemDialogComponent,
    RemoveListDialogComponent,
    EditListDialogComponent,
    UserDetailsDialogComponent
  ]
})
export class ListsModule {
  static forRoot() {
    return {
      ngModule: ListsModule,
      providers: [
        ListService,
        SocketService
      ]
    };
  }
}

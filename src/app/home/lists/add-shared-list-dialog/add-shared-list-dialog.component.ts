import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ListService } from '../../data/list.service';
import { IList } from '../../data/models/IList';

@Component({
  selector: 'app-add-shared-list-dialog',
  templateUrl: './add-shared-list-dialog.component.html',
  styleUrls: ['./add-shared-list-dialog.component.scss']
})
export class AddSharedListDialogComponent implements OnInit {
  public sharedList: any;

  constructor(private dialogRef: MatDialogRef<AddSharedListDialogComponent>,
    private listService: ListService) {
      this.sharedList = { friendCode: '' };
  }

  ngOnInit() {}
  close(): void {
    this.dialogRef.close();
  }
  add(sharedList: any): void {
    this.listService.addSharedList(sharedList).subscribe((list: IList) => {
      this.dialogRef.close(list);
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { ListService } from '../data/list.service';
import { IList } from '../data/models/IList';
import { MatDialogRef, MatDialog } from '@angular/material';
import { AddListDialogComponent } from './add-list-dialog/add-list-dialog.component';
import { AddSharedListDialogComponent } from './add-shared-list-dialog/add-shared-list-dialog.component';
import { Router } from '@angular/router';
import { RemoveListDialogComponent } from './remove-list-dialog/remove-list-dialog.component';
import * as _ from 'lodash';
import { IRemoveList } from '../data/models/IRemoveList';
import { LoadingDialogComponent } from '../../common/loading-dialog/loading-dialog.component';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnInit {
  public userLists: IList[];
  public sharedLists: IList[];
  private addListDialogRef: MatDialogRef<AddListDialogComponent>;
  private addSharedListDialogRef: MatDialogRef<AddSharedListDialogComponent>;
  private removeListDialogRef: MatDialogRef<RemoveListDialogComponent>;
  private loadingDialogRef: MatDialogRef<LoadingDialogComponent>;

  constructor(private listService: ListService,
    private authService: AuthService,
    private router: Router,
    private dialog: MatDialog) {
    this.userLists = [];
    this.sharedLists = [];
  }
  public logout() {
    this.authService.logout();
  }
  ngOnInit() {
    setTimeout(() => {
      this.openLoading();
      this.listService.fetchUserLists().subscribe((lists: IList[]) => {
        this.userLists = lists;
        this.closeLoading();
      });
      this.listService.fetchSharedLists().subscribe((lists: IList[]) => {
        this.sharedLists = lists;
      });
    });
  }
  openAddList(): void {
    this.addListDialogRef = this.dialog.open(AddListDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: false
    });
    this.addListDialogRef.afterClosed().subscribe((result: IList) => {
      if (result) { this.userLists.push(result); }
    });
  }
  openAddSharedList(): void {
    this.addSharedListDialogRef = this.dialog.open(AddSharedListDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: false
    });
    this.addSharedListDialogRef.afterClosed().subscribe((result: any) => {
      if (result) { this.userLists.push(result); }
    });
  }
  openRemoveList(list: IList, ownList: boolean): void {
    const removeList: IRemoveList = {
      ownList: ownList,
      list: list
    };

    this.removeListDialogRef = this.dialog.open(RemoveListDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      data: removeList,
      autoFocus: false

    });

    this.removeListDialogRef.afterClosed().subscribe((_list: IList) => {
      if (_.indexOf(this.userLists, _list) !== -1) {
        this.userLists = _.filter(this.userLists, (__list: IList) =>  __list._id !== _list._id );
      } else if (_.indexOf(this.sharedLists, _list) !== -1) {
        this.sharedLists = _.filter(this.sharedLists, (__list: IList) =>  __list._id !== _list._id );
      }
    });
  }
  navigateToList(list: IList) {
    this.router.navigate(['home/lists/' + list._id]);
  }
  openLoading(): void {
    this.loadingDialogRef = this.dialog.open(LoadingDialogComponent, {
      closeOnNavigation: true,
      disableClose: true
    });
  }
  closeLoading(): void {
    if (this.loadingDialogRef.id) {
      this.loadingDialogRef.close();
    }
  }
}

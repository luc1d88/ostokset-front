import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RoutingModule } from '../routing.module';
import { MaterialModule } from '../material.module';
import { CommonCustomModule } from '../common/common-custom.module';
import { FormsModule } from '@angular/forms';
import { ItemsModule } from './items/items.module';
import { ListsModule } from './lists/lists.module';
import { RecipesModule } from './recipes/recipes.module';
import { UserModule } from './user/user.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    CommonCustomModule,
    ItemsModule.forRoot(),
    ListsModule.forRoot(),
    RecipesModule.forRoot(),
    UserModule.forRoot()
  ],
  declarations: [
    HomeComponent
  ]
})
export class HomeModule {
  static forRoot() {
    return {
      ngModule: HomeModule,
      providers: []
    };
  }
 }

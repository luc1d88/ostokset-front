import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './data/user.service';
import { LocalStorageService } from 'ngx-webstorage';
import { MatDialogRef, MatDialog } from '@angular/material';
import { TermsDialogComponent } from '../common/terms-dialog/terms-dialog.component';
import { RemoveUserDialogComponent } from './user/remove-user-dialog/remove-user-dialog.component';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public selected: string;
  public user: any;
  private termsOfUseDialogRef: MatDialogRef<TermsDialogComponent>;
  private removeListDialogRef: MatDialogRef<RemoveUserDialogComponent>;

  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
    private storage: LocalStorageService,
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.selected = 'lists';
    this.user = this.storage.retrieve('profile');
    this.checkTermsOfUseApprovement();
  }

  navigateToItems() {
    this.router.navigate(['home/items']);
    this.selected = 'items';
  }

  navigateToLists() {
    this.router.navigate(['home/lists']);
    this.selected = 'lists';
  }

  navigateToRecipes() {
    this.router.navigate(['home/recipes']);
    this.selected = 'recipes';
  }

  navigateToUser() {
    this.router.navigate(['home/user']);
    this.selected = 'user';
  }

  private checkTermsOfUseApprovement() {
    this.userService.getTermsOfUse(this.user).subscribe((terms: any) => {
      if (!terms.approvedTermsOfUse) {
        this.openTermsOfUse(this.user);
      }
    });
  }

  private openTermsOfUse(user: any): void {
    this.termsOfUseDialogRef = this.dialog.open(TermsDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: true,
      data: user
    });
    this.termsOfUseDialogRef.afterClosed().subscribe((result) => {
      if (!result) {
        this.openRemoveUser(this.user);
      }
    });
  }

  private openRemoveUser(user: any): void {
    this.removeListDialogRef = this.dialog.open(RemoveUserDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: false,
      data: user
    });
    this.removeListDialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.authService.logout();
      } else {
        this.authService.logout();
      }
    });
  }
}

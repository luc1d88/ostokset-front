import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { ICategory } from '../../data/models/ICategory';
import { CategoryService } from '../../data/category.service';
import { IItem } from '../../data/models/IItem';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category-dialog.component.html',
  styleUrls: ['./add-category-dialog.component.scss']
})
export class AddCategoryDialogComponent implements OnInit {
  public categories: ICategory[];
  public category: any;

  constructor(private dialogRef: MatDialogRef<AddCategoryDialogComponent>,
    private categoryService: CategoryService) {
      this.categories = [];
      this.category = { name: '' };
  }
  close(): void {
    this.dialogRef.close();
  }
  add(item: IItem): void {
    this.categoryService.createCategory({ name: item.name }).subscribe((_category: ICategory) => {
      this.category = _category;
      this.dialogRef.close(_category);
    });
  }
  ngOnInit() {  }

}

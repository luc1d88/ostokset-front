import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { ICategory } from '../../data/models/ICategory';
import { CategoryService } from '../../data/category.service';
import { ItemService } from '../../data/item.service';
import { IItem } from '../../data/models/IItem';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item-dialog.component.html',
  styleUrls: ['./add-item-dialog.component.scss']
})
export class AddItemDialogComponent implements OnInit {
  public categories: ICategory[];
  public item: IItem;

  constructor(private dialogRef: MatDialogRef<AddItemDialogComponent>,
    private itemService: ItemService,
    private categoryService: CategoryService) {
      this.categories = [];
      this.item = { name: '', category: '' };
  }
  getCategories(): void {
    this.categoryService.fetchCategories().subscribe((categories: ICategory[]) => {
      this.categories = categories;
    });
  }
  close(): void {
    this.dialogRef.close();
  }
  add(item: IItem): void {
    this.itemService.createItem({ name: item.name, category: item.category }).subscribe((_item: IItem) => {
      this.item = _item;
      this.dialogRef.close(this.item);
    });
  }
  ngOnInit() {
    this.getCategories();
  }

}

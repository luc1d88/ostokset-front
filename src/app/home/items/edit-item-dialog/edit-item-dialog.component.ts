import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { ICategory } from '../../data/models/ICategory';
import { CategoryService } from '../../data/category.service';
import { ItemService } from '../../data/item.service';
import { IItem } from '../../data/models/IItem';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item-dialog.component.html',
  styleUrls: ['./edit-item-dialog.component.scss']
})
export class EditItemDialogComponent implements OnInit {
  public categories: ICategory[];
  public item: IItem;
  @ViewChild('name') inputElementRef: ElementRef;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IItem,
    private dialogRef: MatDialogRef<EditItemDialogComponent>,
    private itemService: ItemService,
    private categoryService: CategoryService) {
      this.categories = [];
      this.item = data;
  }
  getCategories(): void {
    this.categoryService.fetchCategories().subscribe((categories: ICategory[]) => {
      this.categories = categories;
    });
  }
  close(): void {
    this.dialogRef.close();
  }
  remove(item: IItem): void {
    this.itemService.deleteItem(this.item).subscribe((_item: IItem) => {
      this.item = _item;
      this.dialogRef.close(this.item);
    });
  }
  edit(item: IItem): void {
    this.itemService.updateItem(this.item).subscribe((_item: IItem) => {
      this.item = _item;
      this.dialogRef.close(this.item);
    });
  }
  ngOnInit() {
    this.getCategories();
    this.inputElementRef.nativeElement.blur();
    this.inputElementRef.nativeElement.focus();
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemService } from '../data/item.service';
import { MaterialModule } from '../../material.module';
import { ItemsComponent } from './items.component';
import { AddItemDialogComponent } from './add-item-dialog/add-item-dialog.component';
import { EditItemDialogComponent } from './edit-item-dialog/edit-item-dialog.component';
import { AddCategoryDialogComponent } from './add-category-dialog/add-category-dialog.component';
import { CommonCustomModule } from '../../common/common-custom.module';
import { FormsModule } from '@angular/forms';
import { CategoryService } from '../data/category.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CommonCustomModule,
    MaterialModule
  ],
  declarations: [
    ItemsComponent,
    AddItemDialogComponent,
    EditItemDialogComponent,
    AddCategoryDialogComponent
  ],
  entryComponents: [
    AddItemDialogComponent,
    EditItemDialogComponent,
    AddCategoryDialogComponent,
  ]
})
export class ItemsModule {
  static forRoot() {
    return {
      ngModule: ItemsModule,
      providers: [
        ItemService,
        CategoryService
      ]
    };
  }
}

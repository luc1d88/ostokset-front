import { Component, OnInit } from '@angular/core';
import { ICategory } from '../data/models/ICategory';
import { CategoryService } from '../data/category.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { AddItemDialogComponent } from './add-item-dialog/add-item-dialog.component';
import { ItemService } from '../data/item.service';
import { IItem } from '../data/models/IItem';
import * as _ from 'lodash';
import { AddCategoryDialogComponent } from './add-category-dialog/add-category-dialog.component';
import { EditItemDialogComponent } from './edit-item-dialog/edit-item-dialog.component';
import { LoadingDialogComponent } from '../../common/loading-dialog/loading-dialog.component';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  private originalItems: IItem[];
  public items;
  public categories;
  public search;
  public tabsClosed = false;
  private addItemDialogRef: MatDialogRef<AddItemDialogComponent>;
  private addCategoryDialogRef: MatDialogRef<AddCategoryDialogComponent>;
  private editItemDialogRef: MatDialogRef<EditItemDialogComponent>;
  private loadingDialogRef: MatDialogRef<LoadingDialogComponent>;

  constructor(
    private itemService: ItemService,
    private authService: AuthService,
    private categoryService: CategoryService,
    private dialog: MatDialog) {
  }
  ngOnInit() {
    this.getData();
  }
  public logout() {
    this.authService.logout();
  }
  getData(): void {
    setTimeout(() => {
      this.openLoading();
      this.categoryService.fetchCategories().subscribe((categories: ICategory[]) => {
        this.categories = categories;
        this.itemService.fetchItems().subscribe((items: IItem[]) => {
          this.originalItems = _.map(items, item => Object.assign({}, item));
          this.items = this.mapItems(items, this.categories);
          this.closeLoading();
        });
      });
    });
  }
  mapItems(items, categories): any[] {
    this.tabsClosed = false;
    const lookup = _.keyBy(this.categories, '_id');
    const _items = _.map(items, item => Object.assign({}, item));
    const unique = _.map(_.uniqBy(_items, 'category'), category => _.merge(category, lookup[category.category]));
    return  _.forEach(unique, category => {
      category.closed = this.tabsClosed;
      category.items =  _.filter(items, { 'category' : category._id });
    });
  }
  filterItems(filter): void {
    if (filter.length < 1) {
      this.items = this.mapItems(this.originalItems, this.categories);
    } else {
      let items = [...this.originalItems];
      items = _.filter(items, item => item.name.toLowerCase().indexOf(filter.target.value.toLowerCase()) === 0);
      this.items = this.mapItems(items, this.categories);
    }
  }
  openAddItem(): void {
    this.addItemDialogRef = this.dialog.open(AddItemDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: false
    });
    this.addItemDialogRef.afterClosed().subscribe((result: IItem) => {
      if (result) {
        this.originalItems.push(result);
        const _items = _.map(this.originalItems, item => Object.assign({}, item));

        this.items = this.mapItems(_items, this.categories);
      }
    });
  }
  openAddCategory(): void {
    this.addCategoryDialogRef = this.dialog.open(AddCategoryDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      autoFocus: false
    });
    this.addCategoryDialogRef.afterClosed().subscribe((result: ICategory) => {
      if (result) {
        this.categories.push(result);
      }
    });
  }
  editItem(item: IItem): void {
    this.editItemDialogRef = this.dialog.open(EditItemDialogComponent, {
      width: '300px',
      closeOnNavigation: true,
      disableClose: true,
      data: item,
      autoFocus: false

    });
    this.editItemDialogRef.afterClosed().subscribe((result: IItem) => {
      if (result) {
        this.getData();
      }
    });
  }
  openLoading(): void {
    this.loadingDialogRef = this.dialog.open(LoadingDialogComponent, {
      closeOnNavigation: true,
      disableClose: true
    });
  }
  closeLoading(): void {
    this.loadingDialogRef.close();
  }
  closeTabs(): void {
    this.tabsClosed = !this.tabsClosed;
    _.forEach(this.items, category => {
      category.closed = this.tabsClosed;
    });
  }
}

import { Component, OnInit, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';
@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss']
})
export class ErrorMessageComponent implements OnInit {
  public errorMessage: string;

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {
    this.errorMessage = data;
  }

  ngOnInit() {

  }

}

import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { ErrorMessageComponent } from './error-message/error-message.component';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private snackBar: MatSnackBar) {}
  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return <any> next.handle(req).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) { }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 500) {
          console.log('HTTP 500 error in Interceptor');
        } else if (err.status === 409) {
          this.snackBar.openFromComponent(ErrorMessageComponent, {
            duration: 2000,
            data: err.error
          });
        }
      }
    });
  }
}

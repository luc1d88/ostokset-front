import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-bottom-menu',
  templateUrl: './bottom-menu.component.html',
  styleUrls: ['./bottom-menu.component.scss']
})
export class BottomMenuComponent implements OnInit {
  @Input() active: string;

  @Output() firstClicked: EventEmitter<boolean>  = new EventEmitter<boolean>();
  @Output() secondClicked: EventEmitter<boolean>  = new EventEmitter<boolean>();
  @Output() thirdClicked: EventEmitter<boolean>  = new EventEmitter<boolean>();
  @Output() fourthClicked: EventEmitter<boolean>  = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {}

}

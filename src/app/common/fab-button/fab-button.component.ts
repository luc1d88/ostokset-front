import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { MatBottomSheetRef, MatBottomSheet, MAT_BOTTOM_SHEET_DATA } from '@angular/material';

@Component({
  selector: 'app-fab-button',
  templateUrl: './fab-button.component.html',
  styleUrls: ['./fab-button.component.scss']
})
export class FabButtonComponent implements OnInit {
  @Input() firstTitle: string;
  @Input() secondTitle: string;
  @Input() thirdTitle: string;
  @Input() fourthTitle: string;

  @Input() firstIcon: string;
  @Input() secondIcon: string;
  @Input() thirdIcon: string;
  @Input() fourthIcon: string;

  @Output() firstClicked: EventEmitter<boolean>  = new EventEmitter<boolean>();
  @Output() secondClicked: EventEmitter<boolean>  = new EventEmitter<boolean>();
  @Output() thirdClicked: EventEmitter<boolean>  = new EventEmitter<boolean>();
  @Output() fourthClicked: EventEmitter<boolean>  = new EventEmitter<boolean>();

  private content: any;

  constructor(private bottomSheet: MatBottomSheet) { }

  ngOnInit() {
    this.content = {
      firstTitle: this.firstTitle,
      firstIcon: this.firstIcon,
      secondTitle: this.secondTitle,
      secondIcon: this.secondIcon,
      thirdTitle: this.thirdTitle,
      thirdIcon: this.thirdIcon,
      fourthTitle: this.fourthTitle,
      fourthIcon: this.fourthIcon
    };
  }

  public openBottomSheet(): void {
    this.bottomSheet.open(FabButtonListComponent, { data: this.content})
    .afterDismissed().subscribe((result) => {
      if (result === 1) { this.buttonFirstClicked(); } else if (result === 2 ) {
        this.buttonSecondClicked();
      } else if (result === 3) {
        this.buttonThirdClicked();
      } else if (result === 4) {
        this.buttonFourthClicked();
      }
    });
  }

  private buttonFirstClicked() {
    this.firstClicked.emit(true);
  }
  private buttonSecondClicked() {
    this.secondClicked.emit(true);
  }
  private buttonThirdClicked() {
    this.thirdClicked.emit(true);
  }
  private buttonFourthClicked() {
    this.fourthClicked.emit(true);
  }
}

@Component({
  selector: 'app-fab-button-list',
  templateUrl: './fab-button-list.component.html',
  styleUrls: ['./fab-button-list.component.scss']
})
export class FabButtonListComponent {
  constructor(
    private fabButtonListRef: MatBottomSheetRef<FabButtonListComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {}

  public buttonClicked(value: number): void {
    this.fabButtonListRef.dismiss(value);
  }
  public close() {
    this.fabButtonListRef.dismiss();
  }
}

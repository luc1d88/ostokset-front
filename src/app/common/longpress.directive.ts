import {
  Directive,
  Input,
  Output,
  EventEmitter,
  HostBinding,
  HostListener
} from '@angular/core';

@Directive({
  selector: '[appLongPress]'
})
export class LongpressDirective {
  @Input() duration = 500;

  @Output() longPress: EventEmitter<any> = new EventEmitter();

  private pressing: boolean;
  private longPressing: boolean;
  private timeout: any;

  @HostListener('mousedown', ['$event']) onMouseDown(event) {
    this.timeout = setTimeout(() => {
      this.longPressing = true;
      this.longPress.emit(true);
    }, 300);
  }
  @HostListener('mouseup') onMouseUp() {
    clearTimeout(this.timeout);
  }
  @HostListener('touchstart', ['$event']) onTouchStart(event) {
    this.timeout = setTimeout(() => {
      this.longPressing = true;
      this.longPress.emit(true);
    }, 300);
  }
  @HostListener('touchend') onTouchEnd() {
    clearTimeout(this.timeout);
  }
  @HostListener('touchmove') onTouchMove() {
    clearTimeout(this.timeout);
  }
}

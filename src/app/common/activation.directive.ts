import {
  Directive,
  Input,
  Output,
  EventEmitter,
  HostBinding,
  HostListener
} from '@angular/core';

@Directive({
  selector: '[appActivation]'
})
export class ActivationDirective {
  @Output() activation: EventEmitter<any> = new EventEmitter();

  @HostListener('window:blur') onInActivate() {
    this.activation.emit(false);
  }
  @HostListener('window:focus') onActivate() {
    this.activation.emit(true);
  }
}

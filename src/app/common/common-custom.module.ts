import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingDialogComponent } from './loading-dialog/loading-dialog.component';
import { MaterialModule } from '../material.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor } from './error.interceptor';
import { FabButtonComponent, FabButtonListComponent } from './fab-button/fab-button.component';
import { LongpressDirective } from './longpress.directive';
import { ActivationDirective } from './activation.directive';
import { BottomMenuComponent } from './bottom-menu/bottom-menu.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ErrorMessageComponent } from './error-message/error-message.component';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { environment } from '../../environments/environment';
import { TermsDialogComponent } from './terms-dialog/terms-dialog.component';
import { FormsModule } from '@angular/forms';
import { SocketNotificationComponent } from './socket-notification/socket-notification.component';

const config: SocketIoConfig = { url: environment.url, options: {} };

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    SocketIoModule.forRoot(config)
  ],
  declarations: [
    LoadingDialogComponent,
    FabButtonComponent,
    BottomMenuComponent,
    NavBarComponent,
    LongpressDirective,
    ErrorMessageComponent,
    FabButtonListComponent,
    TermsDialogComponent,
    ActivationDirective,
    SocketNotificationComponent
  ],
  exports: [
    FabButtonComponent,
    LoadingDialogComponent,
    LongpressDirective,
    ActivationDirective,
    BottomMenuComponent,
    NavBarComponent,
    SocketNotificationComponent
  ],
  entryComponents: [
    LoadingDialogComponent,
    ErrorMessageComponent,
    FabButtonListComponent,
    TermsDialogComponent,
    SocketNotificationComponent
  ]
})
export class CommonCustomModule {
  static forRoot() {
    return {
      ngModule: CommonCustomModule,
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorInterceptor,
          multi: true
        },
      ]
    };
}}

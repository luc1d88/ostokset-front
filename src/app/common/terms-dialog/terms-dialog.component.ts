import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from '../../home/data/user.service';
import { IUser } from '../../home/data/models/IUser';

@Component({
  selector: 'app-terms-dialog',
  templateUrl: './terms-dialog.component.html',
  styleUrls: ['./terms-dialog.component.scss']
})
export class TermsDialogComponent implements OnInit {
  public usageTerms: boolean;
  public user: IUser;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IUser,
    private userService: UserService,
    private dialogRef: MatDialogRef<TermsDialogComponent>) { 
    this.usageTerms = false;
    this.user = data;
  }

  ngOnInit() {

  }
  decline(): void {
    this.dialogRef.close(false);
  }
  onApprove(): void {
    this.userService.updateTermsOfUse(this.user, true).subscribe((approvement: boolean) => {
      this.dialogRef.close(true);
    })
  }
}

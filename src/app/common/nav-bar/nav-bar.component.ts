import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  @Input() title: string;
  @Input() showBack: boolean;
  @Input() previousRoute: string;
  @Input() rightText: boolean;
  @Input() rightIcon: string;

  @Output() rightClicked: EventEmitter<boolean> = new EventEmitter();

  constructor(private route: Router) {}

  back() {
    this.route.navigate([this.previousRoute]);
  }
  onRightClick() {
    this.rightClicked.emit(true);
  }
  ngOnInit() {}

}

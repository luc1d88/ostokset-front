export interface IAuthResponse {
  success: boolean;
  token: string;
  profile: any;
}

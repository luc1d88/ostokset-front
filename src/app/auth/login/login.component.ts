import { Component, OnInit, AfterViewInit, NgZone } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { LocalStorageService } from 'ngx-webstorage';

declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  public auth2: any;
  private user: any;
  
  
  constructor(
    private storage: LocalStorageService,
    private authService: AuthService,
    private zone: NgZone,
    private sanitizer: DomSanitizer,
    private iconRegistry: MatIconRegistry,
    private router: Router) {
      iconRegistry.addSvgIcon('facebook', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/facebook.svg'));
      iconRegistry.addSvgIcon('google', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/google.svg'));
    }

  ngOnInit() {}
  ngAfterViewInit() {
    this.googleInit();
  }
  public googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '1098072172471-mf12a301puc46srqjv2i9i86ao93g56e.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });
    });
  }
  public loginWithGoogle() {
    let googleAuth = gapi.auth2.getAuthInstance();
    googleAuth.then(() => {
      googleAuth.signIn({scope: 'profile email'}).then(googleUser => {
        const token = googleUser.getAuthResponse().id_token;
        this.authService.loginWithGoogle(token).subscribe((isAuthenticated: boolean) => {
          if (isAuthenticated) {
            this.zone.run(() => {
              this.router.navigate(['home']);
            }); 
          }
        });
      });
    });
  }

   public loginWithFacebook() {
    this.authService.loginWithFacebook().subscribe((isAuthenticated: boolean) => {
      if (isAuthenticated) {
        this.router.navigate(['home']);
      }
    });
  }
}

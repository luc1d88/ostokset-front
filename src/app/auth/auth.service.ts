import { Injectable } from '@angular/core';
import { FacebookService, InitParams, LoginResponse, LoginOptions } from 'ngx-facebook';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { IAuthResponse } from './models/IAuthResponse';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  private _isAuthenticated: boolean;
  private _token: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    private fb: FacebookService,
    private storage: LocalStorageService) {
    const initParams: InitParams = {
      appId: '163359801016795',
      xfbml: true,
      version: 'v2.8'
    };
    fb.init(initParams);
    this._isAuthenticated = false;
  }
  loginWithGoogle(token) {
    return new Observable((observer) => {
      this.http.post('/auth/social/google',
      { token: token }).subscribe((auth: IAuthResponse) => {
        this.storage.store('token', auth.token);
        this.storage.store('profile', auth.profile);
        this._isAuthenticated = true;
        this._token = auth.token;
        observer.next(true);
        observer.complete();
      });
    });
  }
  loginWithFacebook() {
    return new Observable((observer) => {
      const loginOptions: LoginOptions = {
        scope: 'email, public_profile'
      };
      this.fb.login(loginOptions)
      .then((response: LoginResponse) => {
        this.http.post('/auth/social/facebook',
        {token: response.authResponse.accessToken}).subscribe((auth: IAuthResponse) => {
          this.storage.store('token', auth.token);
          this.storage.store('profile', auth.profile);
          this._isAuthenticated = true;
          this._token = auth.token;
          observer.next(true);
          observer.complete();
        });
      })
      .catch((error: any) => {
        observer.error();
        observer.complete();
      });
    });
  }
  verify() {
    return new Observable((observer) => {
      let token: string;
      token = this.storage.retrieve('token');
      if (token) {
        this.http.post('/auth/verify', { token: token }).subscribe((isAuthenticated: boolean) => {
          observer.next(isAuthenticated);
          this._isAuthenticated = isAuthenticated;
          this._token = token;
          observer.complete();
        }, (e) => {
          observer.error();
          observer.complete();
        });
      } else {
        observer.next(false);
        observer.complete();
      }
    });
  }
  logout() {
    this.storage.clear('token');
    this.storage.clear('profile');
    this._isAuthenticated = false;
    this.router.navigate(['auth/login']);
  }
  isAuthenticated(): boolean {
    return this._isAuthenticated;
  }
  getToken(): string {
    return this._token;
  }
}

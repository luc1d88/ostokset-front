import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Subscription } from 'rxjs/Subscription';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoadingDialogComponent } from '../common/loading-dialog/loading-dialog.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {
  private authObserver: Subscription;
  private dialogRef: MatDialogRef<LoadingDialogComponent>;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private authService: AuthService) {
  }
  openLoading(): void {
    this.dialogRef = this.dialog.open(LoadingDialogComponent, {
      height: '150px',
      width: '150px',
      closeOnNavigation: true,
      disableClose: true
    });
  }
  closeLoading(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.checkAuthentication();
  }
  ngOnDestroy() {
    this.authObserver.unsubscribe();
  }
  checkAuthentication() {
    setTimeout(() => {
      this.openLoading();
      this.authObserver = this.authService.verify().subscribe((isAuthenticated: boolean) => {
        if (isAuthenticated) {
          this.router.navigate(['home']);
        }
        this.closeLoading();
      }, (e) => {
        this.closeLoading();
      });
    });
  }
}

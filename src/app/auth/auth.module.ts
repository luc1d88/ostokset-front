import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';
import { SignupComponent } from './signup/signup.component';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { RoutingModule } from '../routing.module';
import { MaterialModule } from '../material.module';
import { HttpClientModule } from '@angular/common/http';
import { FacebookModule } from 'ngx-facebook';
import { Ng2Webstorage } from 'ngx-webstorage';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../auth/token.interceptor';
import { AuthInterceptor } from '../auth/auth.interceptor';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    MaterialModule,
    HttpClientModule,
    Ng2Webstorage,
    FacebookModule.forRoot(),
  ], 
  declarations: [LoginComponent, AuthComponent, SignupComponent],
  exports: [AuthComponent]

})
export class AuthModule {
  static forRoot() {
    return {
      ngModule: AuthModule,
      providers: [
        AuthService,
        AuthGuard,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true
        }
      ]
    };
  }
}

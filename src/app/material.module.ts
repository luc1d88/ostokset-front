import { NgModule } from '@angular/core';

import {
  MatButtonModule,
  MatMenuModule,
  MatListModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatDialogModule,
  MatSelectModule,
  MatProgressSpinnerModule,
  MatAutocompleteModule,
  MatChipsModule,
  MatSliderModule,
  MatRadioModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatBottomSheetModule
} from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule,
    MatMenuModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatSliderModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatBottomSheetModule
  ],
  exports: [
    MatButtonModule,
    MatMenuModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatSliderModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatBottomSheetModule
  ]
})

export class MaterialModule {}

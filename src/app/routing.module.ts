import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { ItemsComponent } from './home/items/items.component';
import { ListsComponent } from './home/lists/lists.component';
import { AuthGuard } from './auth/auth.guard';
import { RecipesComponent } from './home/recipes/recipes.component';
import { ListComponent } from './home/lists/list/list.component';
import { UserComponent } from './home/user/user.component';
import { RecipeComponent } from './home/recipes/recipe/recipe.component';

const routes: Routes = [
  { path: 'auth', component: AuthComponent,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'signup', component: SignupComponent }
  ]},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'lists', pathMatch: 'full' },
      { path: 'lists', component: ListsComponent },
      { path: 'lists/:id', component: ListComponent },
      { path: 'items', component: ItemsComponent },
      { path: 'recipes', component: RecipesComponent },
      { path: 'recipes/:id', component: RecipeComponent },
      { path: 'user', component: UserComponent }
  ]},
  { path: '**', redirectTo: 'auth' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { 
        enableTracing: false,       // <-- debugging purposes only
        onSameUrlNavigation: 'reload'
      }  
    )
  ],
  exports: [RouterModule]
})
export class RoutingModule { }
